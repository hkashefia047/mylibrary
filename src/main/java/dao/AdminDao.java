package dao;

import model.Admin;
import model.Customer;
import util.DbConnection;
import util.MD5;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AdminDao implements CrudOperation<Admin> {

    public Customer login(String fullName, String password) {
        try {
            DbConnection.getConnection();
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement("select id,fullName from admin where fullName=? AND password=?");
            preparedStatement.setString(1, fullName);
            preparedStatement.setString(2, MD5.getMd5(password));
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Customer customer = new Customer();
                customer.setId(resultSet.getInt("id"));
                customer.setFullName(resultSet.getString("fullName"));
                return customer;
            }
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return null;
    }


    @Override
    public boolean save(Admin admin) {
        try {
            DbConnection.getConnection();
            String save = "insert into admin(fullName,age,address,phone," +
                    "password) values(?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = getPreparedStatement(admin, save);

            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
        return false;
    }

    private PreparedStatement getPreparedStatement(Admin admin, String save) throws SQLException {
        PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(save);
        preparedStatement.setString(1, admin.getFullName());
        preparedStatement.setInt(2, admin.getAge());
        preparedStatement.setString(3, admin.getAddress());
        preparedStatement.setString(4, admin.getPhone());
        preparedStatement.setString(5, MD5.getMd5(admin.getPassword()));
        return preparedStatement;
    }

    private PreparedStatement getPreparedStatement(Admin admin) throws SQLException {
        String update = "update customer set fullName=?,age=?,address=?,phone=?,password=?" +
                " where id=?";
        PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(update);
        preparedStatement.setString(1, admin.getFullName());
        preparedStatement.setInt(2, admin.getAge());
        preparedStatement.setString(3, admin.getAddress());
        preparedStatement.setString(4, admin.getPhone());
        preparedStatement.setString(5, MD5.getMd5(admin.getPassword()));
        return preparedStatement;
    }

    @Override
    public Optional<Admin> findById(Integer id) {
        try {
            DbConnection.getConnection();

            String readById = "select * from customer where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(readById);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Admin admin = getAdmin(resultSet);

                return Optional.of(admin);
            }

        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return Optional.empty();
    }

    private Admin getAdmin(ResultSet resultSet) throws SQLException {
        Admin admin = new Admin();
        admin.setFullName(resultSet.getString("fullName"));
        admin.setAge(resultSet.getInt("age"));
        admin.setAddress(resultSet.getString("address"));
        admin.setPhone(resultSet.getString("phone"));
        return admin;
    }

    @Override
    public List<Admin> findAll() {
        try {
            DbConnection.getConnection();

            String readAll = "select * from admin";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(readAll);
            ResultSet resultSet = preparedStatement.executeQuery();

            List<Admin> all = new ArrayList<>();
            if (resultSet.next()) {
                Admin admin = getAdmin(resultSet);

                all.add(admin);
            }
            return all;
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return null;
    }

    @Override
    public boolean update(Admin admin) {

        try {
            DbConnection.getConnection();

            int i = getPreparedStatement(admin).executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        try {
            DbConnection.getConnection();

            String delete = "delete from admin where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(delete);
            preparedStatement.setInt(1, id);

            int i = preparedStatement.executeUpdate();

            if (i == 1) {
                return true;
            }
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return false;
    }
}

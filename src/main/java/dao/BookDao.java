package dao;

import model.Book;
import util.DbConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class BookDao implements CrudOperation<Book> {

    public boolean save(Book book) {

        try {
            DbConnection.getConnection();
            String save = "insert into Book(title,author,price) values(?,?,?)";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(save);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setInt(3, book.getPrice());

            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
        return false;
    }

    public Optional<Book> findById(Integer id) {

        try {
            DbConnection.getConnection();

            String readById = "select * from Book where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(readById);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                String title = resultSet.getString("title");
                String author = resultSet.getString("author");
                Integer price = resultSet.getInt("price");

                return Optional.ofNullable(new Book().setTitle(title).setAuthor(author).setPrice(price));
            }

        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return Optional.empty();
    }

    public List<Book> findAll() {
        try {
            DbConnection.getConnection();

            String readAll = "select * from Book";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(readAll);
            ResultSet resultSet = preparedStatement.executeQuery();

            List<Book> all = new ArrayList<>();
            if (resultSet.next()) {
                Book book = new Book().setId(resultSet.getInt("id"))
                        .setTitle(resultSet.getString("title"))
                        .setAuthor(resultSet.getString("author"))
                        .setPrice(resultSet.getInt("price"));
                all.add(book);
            }
            return all;

        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }

        return null;
    }

    public boolean update(Book book) {
        try {
            DbConnection.getConnection();

            String update = "update Book set title=?,author=?,price=? where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(update);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setInt(3, book.getPrice());
            preparedStatement.setInt(4, book.getId());

            int i = preparedStatement.executeUpdate();

            if (i == 1) {
                return true;
            }
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return false;
    }

    public boolean delete(Integer id) {
        try {
            DbConnection.getConnection();

            String delete = "delete from Book where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(delete);
            preparedStatement.setInt(1, id);

            int i = preparedStatement.executeUpdate();

            if (i == 1) {
                return true;
            }
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return false;
    }
}

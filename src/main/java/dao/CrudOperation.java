package dao;

import java.util.List;
import java.util.Optional;

public interface CrudOperation <X> {

     boolean save(X x);
     Optional<X>findById(Integer id);
     List<X>findAll();
     boolean update(X x);
     boolean delete(Integer id);
}

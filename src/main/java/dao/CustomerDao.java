package dao;


import model.Customer;
import util.DbConnection;
import util.MD5;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomerDao implements CrudOperation<Customer> {

    //login method
    public Customer login(String fullName, String password) {
        try {
            DbConnection.getConnection();
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement("select id,fullName from customer where fullName=? AND password=?");
            preparedStatement.setString(1, fullName);
            preparedStatement.setString(2, MD5.getMd5(password));
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Customer customer = new Customer();
                customer.setId(resultSet.getInt("id"));
                customer.setFullName(resultSet.getString("fullName"));
                return customer;
            }
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return null;
    }

    @Override
    public boolean save(Customer customer) {
        try {
            DbConnection.getConnection();
            String save = "insert into customer(fullName,age,address,phone," +
                    "cashInHand,password,email) values(?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(save);
            preparedStatement.setString(1, customer.getFullName());
            preparedStatement.setInt(2, customer.getAge());
            preparedStatement.setString(3, customer.getAddress());
            preparedStatement.setString(4, customer.getPhone());
            preparedStatement.setInt(5, customer.getCashInHand());
            preparedStatement.setString(6, MD5.getMd5(customer.getPassword()));
            preparedStatement.setString(7, customer.getEmail());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public Optional<Customer> findById(Integer id) {
        try {
            DbConnection.getConnection();

            String readById = "select * from customer where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(readById);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                Customer customer = getCustomer(resultSet);

                return Optional.of(customer);
            }

        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public List<Customer> findAll() {
        try {
            DbConnection.getConnection();

            String readAll = "select * from customer";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(readAll);
            ResultSet resultSet = preparedStatement.executeQuery();

            List<Customer> all = new ArrayList<>();
            if (resultSet.next()) {
                Customer customer = getCustomer(resultSet);

                all.add(customer);
            }
            return all;
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }

        return null;
    }

    private Customer getCustomer(ResultSet resultSet) throws SQLException {
        Customer customer = new Customer();
        customer.setFullName(resultSet.getString("fullName"));
        customer.setAge(resultSet.getInt("age"));
        customer.setAddress(resultSet.getString("address"));
        customer.setCashInHand(resultSet.getInt("cashInHand"));
        customer.setPhone(resultSet.getString("phone"));
        customer.setPassword(resultSet.getString("password"));
        customer.setEmail(resultSet.getString("email"));
        return customer;
    }

    @Override
    public boolean update(Customer customer) {
        try {
            DbConnection.getConnection();
            String update = "update customer set fullName=?,age=?,address=?,phone=?,cashInHand=?,password=?,email=?" +
                    " where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(update);
            preparedStatement.setString(1, customer.getFullName());
            preparedStatement.setInt(2, customer.getAge());
            preparedStatement.setString(3, customer.getAddress());
            preparedStatement.setString(4, customer.getPhone());
            preparedStatement.setInt(5, customer.getCashInHand());
            preparedStatement.setString(6, MD5.getMd5(customer.getPassword()));
            preparedStatement.setString(7, customer.getEmail());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
        return false;

    }

    @Override
    public boolean delete(Integer id) {
        try {
            DbConnection.getConnection();

            String delete = "delete from customer where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(delete);
            preparedStatement.setInt(1, id);

            int i = preparedStatement.executeUpdate();

            if (i == 1) {
                return true;
            }
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return false;
    }
}

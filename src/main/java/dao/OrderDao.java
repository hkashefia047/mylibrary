package dao;


import model.Order;

import util.DbConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OrderDao implements CrudOperation<Order> {
    @Override
    public boolean save(Order order) {
        try {
            DbConnection.getConnection();
            String save = "insert into `order`(customer_id,book_id,purchaseDate) values(?,?,?)";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(save);
            preparedStatement.setInt(1, order.getCustomer_id());
            preparedStatement.setInt(2, order.getBook_id());
            preparedStatement.setDate(3, order.getPurchaseDate());

            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public Optional<Order> findById(Integer id) {
        try {
            DbConnection.getConnection();

            String readById = "select * from `order` where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(readById);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(new Order().setCustomer_id(resultSet.getInt("customer_id"))
                        .setBook_id(resultSet.getInt("book_id"))
                        .setPurchaseDate(resultSet.getDate("purchaseDate")));

            }
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public List<Order> findAll() {
        try {
            DbConnection.getConnection();

            String readAll = "select * from `order`";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(readAll);

            ResultSet resultSet = preparedStatement.executeQuery();

            List<Order>all=new ArrayList<>();
            if (resultSet.next()) {
                Order order=new Order();
                order.setId(resultSet.getInt("id"));
                order.setCustomer_id(resultSet.getInt("customer_id"));
                order.setPurchaseDate(resultSet.getDate("purchaseDate"));

                 all.add(order);
            }
            return all;

        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }

        return null;
    }

    @Override
    public boolean update(Order order) {
        String updating="update `order` set customer_id=?,book_id=? where id=?";
        try {
            DbConnection.getConnection();
            PreparedStatement preparedStatement=DbConnection.conn.prepareStatement(updating);
            preparedStatement.setInt(1,order.getCustomer_id());
            preparedStatement.setInt(2,order.getBook_id());
            preparedStatement.setInt(3,order.getId());

            if(preparedStatement.executeUpdate()==1){
                return true;
            }
        }catch (SQLException s){
            System.out.println(s.getMessage());
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        try {
            DbConnection.getConnection();

            String delete = "delete from `order` where id=?";
            PreparedStatement preparedStatement = DbConnection.conn.prepareStatement(delete);
            preparedStatement.setInt(1, id);

            int i = preparedStatement.executeUpdate();

            if (i == 1) {
                return true;
            }
        } catch (SQLException s) {
            System.out.println(s.getMessage());
        }
        return false;
    }
}

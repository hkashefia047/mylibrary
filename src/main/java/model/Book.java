package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data @NoArgsConstructor @AllArgsConstructor @Accessors(chain = true)
public class Book {
    private Integer id;
    private String title;
    private String author;
    private Integer price;

}


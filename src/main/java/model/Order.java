package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.sql.Date;
@Accessors(chain = true)
@NoArgsConstructor @AllArgsConstructor @Data
public class Order {
    private Integer id;
    private Integer customer_id;
    private Integer book_id;
    private Date purchaseDate;
}

package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public abstract class Person {
    private Integer id;
    private String fullName;
    private Integer age;
    private String address;
    private String phone;


}

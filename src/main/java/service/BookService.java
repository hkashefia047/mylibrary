package service;

import dao.BookDao;
import dao.CrudOperation;
import model.Book;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class BookService implements ServiceInterface<Book> {

    private final BookDao bookDao;

    public BookService(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @Override
    public boolean save(Book book){
       try {
           bookDao.save(book);
           return true;
       }catch (Exception e){
           System.out.println(e.getMessage());
       }
       return false;
    }

    @Override
    public Book findById(Integer id) {
        if(bookDao.findById(id).isPresent()){
            return bookDao.findById(id).get();
        }else {
            return null;
        }
    }

    @Override
    public List<Book> findAll() {
        try {
            return bookDao.findAll();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public boolean update(Book book) {
        try{
            bookDao.update(book);
        }catch (Exception e){
            e.getMessage();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        try {
            bookDao.delete(id);
            return true;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return false;
    }
}

package service;

import dao.CustomerDao;
import model.Customer;

import java.util.List;

public class CustomerService implements Login<Customer>,ServiceInterface<Customer> {
   private CustomerDao customerDao;

    public CustomerService(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }


    @Override
    public Customer login(String fullName, String password) {
        Customer customer=customerDao.login(fullName,password);
        if(customer!=null){
            return customer;
        }else {
            return null;
        }
    }

    @Override
    public boolean save(Customer customer) {
        if(customerDao.save(customer)){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public Customer findById(Integer id) {
        if(customerDao.findById(id).isPresent()){
            return customerDao.findById(id).get();
        }else {
            return null;
        }
    }

    @Override
    public List<Customer> findAll() {
        try {
            return customerDao.findAll();
        }catch (Exception e){
            e.getMessage();
        }

        return null;
    }

    @Override
    public boolean update(Customer customer) {
        if(customerDao.update(customer)){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean delete(Integer id) {
        if(customerDao.delete(id)){
            return true;
        }else {
            return false;
        }
    }
}

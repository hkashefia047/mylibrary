package service;

public interface Login <X> {
    public X login(String fullName,String password);
}

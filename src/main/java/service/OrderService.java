package service;

import dao.OrderDao;
import model.Order;

import java.util.List;

public class OrderService implements ServiceInterface <Order> {

    private OrderDao orderDao;

    public OrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public boolean save(Order order) {
        if(orderDao.save(order)){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public Order findById(Integer id) {
        if (orderDao.findById(id).isPresent()){
            return orderDao.findById(id).get();
        }
        return null;
    }

    @Override
    public List<Order> findAll() {
        try{
            return orderDao.findAll();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public boolean update(Order order) {
        if(orderDao.update(order)){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean delete(Integer id) {
        if(orderDao.delete(id)){
            return true;
        }else {
            return false;
        }
    }
}

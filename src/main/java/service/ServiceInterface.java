package service;

import java.util.List;
import java.util.Optional;

public interface ServiceInterface <X> {

    boolean save(X x);
    X findById(Integer id);
    List<X> findAll();
    boolean update(X x);
    boolean delete(Integer id);
}
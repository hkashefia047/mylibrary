package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RegisterAdmin extends JFrame implements ActionListener {

    public RegisterAdmin() throws HeadlessException {
        JFrame frame = new JFrame("Register");

        JLabel label ,label1,label2,label3,label4,label6;
        JTextField textField1,textField2,textField3,textField4;
        JPasswordField passwordField;
        JButton button;

        label=new JLabel("Admin Registeration Form:");
        label.setFont(new Font("Serif", Font.BOLD, 20));
        label.setBounds(20,10,300,30);

        label1=new JLabel("fullName");
        textField1=new JTextField(15);
        label1.setBounds(20,70,200,30);
        textField1.setBounds(160,70,200,30);

        label2=new JLabel("age");
        textField2=new JTextField(15);
        label2.setBounds(20,110,200,30);
        textField2.setBounds(160,110,200,30);

        label3=new JLabel("address");
        textField3=new JTextField(150);
        label3.setBounds(20,150,200,30);
        textField3.setBounds(160,150,200,30);

        label4=new JLabel("phone");
        textField4=new JTextField(15);
        label4.setBounds(20,190,200,30);
        textField4.setBounds(160,190,200,30);

        label6=new JLabel("password");
        passwordField=new JPasswordField(15);
        label6.setBounds(20,240,200,30);
        passwordField.setBounds(160,240,200,30);


        button=new JButton("Save");
        button.setBounds(160,290,200,30);
        frame.add(label);
        frame.add(label1);
        frame.add(label2);
        frame.add(label3);
        frame.add(label4);

        frame.add(label6);

        frame.add(textField1);
        frame.add(textField2);
        frame.add(textField3);
        frame.add(textField4);

        frame.add(passwordField);

        frame.add(button);
        frame.setSize(500,500);
        frame.setResizable(false);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
